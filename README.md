# Flatpak Qt5 modules

Provides easy-to-use modules for shipping a Qt5-based application which only
depends on the FreeDesktop runtime with minimal size overhead. Depending on
the selected components the additional size may be as small as 8MiB.

Compared to the KDE Platform runtime at ~200MiB this is a huge benefit in terms
of size if you're using few or none of the KF5. Please be advised that
including QtWebEngine or QtWebKit in your build may easily increase your final
bundle size by 40MiB or more however.

Currently this repository targets Qt 5.10.1, KF 5.54.0 and Plasma 5.12.

## Install

If you're using GIT it's probably the easiest to add this repository as a
submodule:

```
git submodule add https://gitlab.com/ntninja/flatpak-qt5-modules.git qt5
```

Other configurations (such as using the automatically created ZIP) will work
just as well however.

## Usage

The following will assume that the Flatpak Qt5 modules are placed in a
directory named `qt5` next to your app's main manifest (using the
`submodule` command above for instance).

### Add the required sandbox exceptions

This is only required once, but the list may change for future versions
of Qt, KDE or Flatpak.

Add the following to the `"finish-args"` list of your app's manifest:

```
"--env=GI_TYPELIB_PATH=/app/lib/girepository-1.0",
"--env=GST_PLUGIN_SYSTEM_PATH=/app/lib/gstreamer-1.0:/usr/lib/extensions/gstreamer-1.0:/usr/lib/gstreamer-1.0",
"--env=XDG_DATA_DIRS=/app/share:/usr/share:/usr/share/runtime/share:/run/host/share",
"--env=QT_PLUGIN_PATH=/app/lib64/plugins:/app/lib/plugins:/usr/share/runtime/lib/plugins",
"--env=DCONF_USER_CONFIG_DIR=.config/dconf",
"--filesystem=xdg-config/kdeglobals:ro",
"--filesystem=xdg-run/dconf",
"--filesystem=~/.config/dconf:ro",
"--talk-name=com.canonical.AppMenu.Registrar"
```

 1. The first part (environment variables) help Qt and other possibly installed
    components cope with the way Flatpak's file system is structured.
 2. The second part (filesystem and D-Bus exceptions) allow your app to achieve
    better desktop integration by providing access to features that are not
    currently available via portals. Except this list to shrink over time.

### Enable extra safety features for your build (Recommended)

The following options may improve the security of your app at a minimal
runtime cost and adding them is therefor recommended for all apps
(note that several other options like `-O`, `-g` and `-L` are only added
to ensure feature-pairity with the defaults provided by Flatpak):

```
"build-options": {
	"cflags": "-Os -g -fstack-protector-strong -D_FORTIFY_SOURCE=2",
	"cxxflags": "-Os -g -fstack-protector-strong -D_FORTIFY_SOURCE=2",
	"ldflags": "-fstack-protector-strong -Wl,-z,relro,-z,now -L/app/lib",
	"env": {
		"V": "1"
	}
}
```

### Add the modules you wish to use

Each can JSON-file in this repository can be added as a normal Flatpak module
to your `flatpak-builder` manifest. In order to get started quickly and relief
you of the burden of tracking all the module dependencies yourself, some sets
modules are provided for you:

#### `qt5-standard-modules.json`

This set contains modules that are useful for your avarage `QWidget`-based app:

 1. QtBase (provding QtCore, QtGui, QtWidgets, IBus IME support, GTK theming and more)
 2. QtSvg (support for SVG images)
 3. QtWayland (support for the modern Wayland display server)
 4. `fcitx-platforminputcontext` (FCITX IME support)

Using this module is recommended for all apps.

Size: ~8MiB

#### `qt5-kde-interation-modules.json`

This set contains modules that are useful for better integration in KDE Plasma:

 1. `frameworkintegration`
    (Provides some KDE-specific convience features in Qt apps)
 2. `breeze` theme engine
    (Provides a more native look and feel with KDE's default theme)
 3. `plasma-integration`
    (Applies several tweaks to Qt apps based on the current desktop
     configuration)

Using this module is recommend for most apps without stringent size
constraints.

Size: ~9MiB

### `pyqt5.json`

Not really a module set, but still very useful if your app uses Python.

Size: ~2,5MiB

## Contribute

Please open [Open an Issue](https://gitlab.com/ntninja/flatpak-qt5-modules/issues/new)
if you experience any problems. Be advised however that I will only fix
important issues on short notice and that I fully reserve the right to
prioritze issues based on my on interest and time. Some issue may also never
be addressed from my side.

If you want your issue to be fixed quickly please do so yourself and submit
a *Merge Request* instead. I'm happy to provide guidance if you get stuck on
anything.

## License

[LGPL-3+](LICENSE.md) © Alexander Schlarb

Readme written based on the guidelines of
[Standard Readme](https://github.com/RichardLitt/standard-readme).